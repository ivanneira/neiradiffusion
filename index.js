const fs = require('fs');
var bmp = require("bmp-js");

console.log(getBmpTensor("color.bmp"))

function getBmpTensor(filename){


    let bufferData  = fs.readFileSync(filename);
    let bmpData = bmp.decode(bufferData);
    
    let bmpTensor = [];
    let count = 0;
    let bufferTensor = [];
    
    for(i = 0; i < 100; i++){
    
        bufferTensor = bufferTensor.concat(bmpData.data[i]);
    
        if(count < 3){
    
            count++;
        }else{
            count = 0;
            bmpTensor.push(bufferTensor)
            bufferTensor = [];
        }
    }
    
    return bmpTensor;

}
